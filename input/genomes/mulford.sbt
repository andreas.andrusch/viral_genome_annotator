Submit-block ::= {
  contact {
    contact {
      name name {
        last "Schrick",
        first "Livia",
        middle "",
        initials "",
        suffix "",
        title ""
      },
      affil std {
        affil "Robert Koch Institute",
        div "ZBS 1: Highly Pathogenic Viruses",
        city "Berlin",
        sub "Berlin",
        country "Germany",
        street "Seestr. 10",
        email "schrickl@rki.de",
        postal-code "13353"
      }
    }
  },
  cit {
    authors {
      names std {
        {
          name name {
            last "Schrick",
            first "Livia",
            middle "",
            initials "",
            suffix "",
            title ""
          }
        },
        {
          name name {
            last "Tausch",
            first "Simon",
            middle "",
            initials "H.",
            suffix "",
            title ""
          }
        },
        {
          name name {
            last "Dabrowski",
            first "Piotr",
            middle "",
            initials "W.",
            suffix "",
            title ""
          }
        },
        {
          name name {
            last "Damaso",
            first "Clarissa",
            middle "",
            initials "R.",
            suffix "",
            title ""
          }
        },
        {
          name name {
            last "Esparza",
            first "Jose",
            middle "",
            initials "",
            suffix "",
            title ""
          }
        },
        {
          name name {
            last "Nitsche",
            first "Andreas",
            middle "",
            initials "",
            suffix "",
            title ""
          }
        }
      },
      affil std {
        affil "Robert Koch Institute",
        div "ZBS 1: Highly Pathogenic Viruses",
        city "Berlin",
        sub "Berlin",
        country "Germany",
        street "Seestr. 10",
        postal-code "13353"
      }
    }
  },
  subtype new
}
Seqdesc ::= pub {
  pub {
    gen {
      cit "unpublished",
      authors {
        names std {
          {
            name name {
              last "Schrick",
              first "Livia",
              middle "",
              initials "",
              suffix "",
              title ""
            }
          },
          {
            name name {
              last "Tausch",
              first "Simon",
              middle "",
              initials "H.",
              suffix "",
              title ""
            }
          },
          {
            name name {
              last "Dabrowski",
              first "Piotr",
              middle "",
              initials "W.",
              suffix "",
              title ""
            }
          },
          {
            name name {
              last "Damaso",
              first "Clarissa",
              middle "",
              initials "R.",
              suffix "",
              title ""
            }
          },
          {
            name name {
              last "Esparza",
              first "Jose",
              middle "",
              initials "",
              suffix "",
              title ""
            }
          },
          {
            name name {
              last "Nitsche",
              first "Andreas",
              middle "",
              initials "",
              suffix "",
              title ""
            }
          }
        }
      },
      title "An early American smallpox vaccine based on horsepox"
    }
  }
}
